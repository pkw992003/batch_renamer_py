import tkinter as tk
import pathlib  # For getting current path
import os       # For rename file
import sys




# Prefix method
def prefix_name(ls, prefix = ''):
    for i in range(len(ls)):
        ls[i] = prefix + ls[i]
        
def suffix_name(ls, suffix = ''):
    for i in range(len(ls)):
        ls[i] = ls[i][:ls[i].rfind('.')] + suffix + ls[i][ls[i].rfind('.'):]

def del_name(ls, remove_char = ''):
    for i in range(len(ls)):
        ls[i] = ls[i].replace(remove_char,'')

def ordered_number(ls, start=0,padding_zero = True):
    list_len = len(str(len(ls)))
    
    for i in range(len(ls)):
        pad = ''
        if padding_zero == True:
            while (len(pad+str(i))) < list_len:
                    pad = pad+'0'
        ls[i] = pad+str(i+start)+ls[i][ls[i].rfind('.'):]

def executeRename(old_name_list, new_name_list):
    for i in range(len(new_name_list)):
        os.rename(old_name_list[i],new_name_list[i])


current_path = pathlib.Path().resolve() # Get current path
list_files = os.listdir(current_path)   # Get list of files
exclude_files = [sys.argv[0],'.git']    # set a list of excluded file

for i in range(len(exclude_files)):
    list_files.remove(exclude_files[i])          # Remove current file name from the list
    
new_list_files = list_files.copy()      # deep copy the file list

# select the method 
#---------------------
# root = tk.Tk()
# root.title('Batch renamer')
# root.geometry('500x300')


# title = tk.Label(root, text="Batch Renamer")
# title.grid(row=0,column=0)

# root.mainloop()


print('Batch Renamer by Kiwi Poon')
print('---------------------------')
print('1 Prefix eg. file => file1.txt; file2.txt; file3.txt')
print('2 Suffix eg. file => 1file.txt; 2file.txt; 3file.txt')
print('3 Ordered Number eg. 1.txt; 2.txt; 3.txt')
print('4 Delete specific text in the file name')
print('Enter the number that not in option to exit')
print('---------------------------')
print('Current Path : ' + str(current_path))

selection = input('Please select function with the function number : \n')

if selection == '1':
    alt_name = input('Please enter the PREFIX text : \n')
    prefix_name(new_list_files,alt_name)
elif selection == '2':
    alt_name = input('Please enter the SUFFIX text : \n')
    prefix_name(new_list_files,alt_name)
elif selection == '3':
    padding = input('Do you want to pad zero at the front : 1 => Yes; 2 => No \n')
    padding_tf = None
    
    if padding == 'Y' or padding =='y' or padding == '1':
        padding_tf = True
    else:
        padding_tf = False
        
    ordered_number(new_list_files,0,padding_tf)
elif selection == '4':
    alt_name = input('Please enter the text you want to delete : \n')
    del_name(new_list_files,alt_name)
else:
    exit()
    

print('Your files will change to : ')
if len(new_list_files) <= 5:
    for filename in new_list_files:
        print(filename)
else :
    for i in range(len(new_list_files)):
        print(new_list_files[i])
    print('And more...')
comfirm = input('Comfirm to change? 1 => Yes; 2 => No \n')
if(comfirm == '1'):
    executeRename(list_files,new_list_files);
    print('Rename successfully')
else:
    exit()




# prefix_name(new_list_files, 'aa')
# suffix_name(new_list_files, 'aa')

# del_name(new_list_files,'ls')

# order_number(new_list_files,1)


# Print compare
print(list_files)
print(new_list_files)


# execute the rename
# for i in range(len(new_list_files)):
#     os.rename(list_files[i],new_list_files[i])


# print(list_files)
# print(len(list_files))
# print(sys.argv[0])
# print('Hellollollo.exe'.rfind('.'))