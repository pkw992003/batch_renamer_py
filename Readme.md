# Simple Batch File Renamer by Kiwi Poon

This is a file renamer allow user rename all the file inside a folder at once.

## Usage

Copy the 'batch_renamer.py' to the target folder. The script renames all the files inside the folders.
the script keep the file extension name automatically.
There are 5 function can rename your files.

`python batch_renamer.py`

- Prefix\
Adding the user specified text at the front of the file name.\
eg: user specify 'my'; original file name is 'file.txt'; the result will be "myfile.txt"\

- Suffix\
Adding the user specified text at the end of the file name.\
eg: user specify 'my'; original file name is 'file.txt'; the result will be "filemy.txt"\

- Ordered Number\
The files in the same folder will rename as ordered number.\
The zero padding is optional.\
eg: '1.txt'; '2.txt'; '3.txt'\

- Delete the specific text inside the file name\
To delete the specific text inside the file\
eg: 'files1.txt'; 'files2.txt'; 'files3.txt';\
user input : fil\
the result : 'es1.txt'; 'es2.txt'; 'es3.txt';\

